using UnityEngine;
using TMPro; // TextMeshPro hinzufügen
using System.Collections.Generic;

public class QuestionManager : MonoBehaviour
{
    public TMP_Text questionText;
    public TMP_Text answerText1;
    public TMP_Text answerText2;


    public TMP_Text CounterLeft;
    public TMP_Text CounterRight;


    private List<string> questions = new List<string> {
        "Frage 1",
        "Frage 2",
        "Frage 3"
    };

    private List<string> answers1 = new List<string> {
        "Antwort 1A",
        "Antwort 2A",
        "Antwort 3A"
    };

    private List<string> answers2 = new List<string> {
        "Antwort 1B",
        "Antwort 2B",
        "Antwort 3B"
    };

    private int currentQuestionIndex = 0;

    void Start()
    {
        LoadQuestion();
    }

    
    public void LoadQuestion()
    {
        if (currentQuestionIndex >= questions.Count)
        {
            currentQuestionIndex = 0; // Zurücksetzen oder Spielende Logik
        }

        questionText.text = questions[currentQuestionIndex];
        answerText1.text = answers1[currentQuestionIndex];
        answerText2.text = answers2[currentQuestionIndex];
    }

    public void OnCounterIncreased()
    {
 



        currentQuestionIndex++;
        LoadQuestion();
    }
}
