using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class ProcessMaskPixel : MonoBehaviour
{
    [SerializeField] Vector2 headsScaling = new Vector2(1, 1);
    [SerializeField] Vector2 headsOffset = new Vector2(0, 0);
    [SerializeField] ComputeShader computeShader = null;
    [SerializeField] Compositor compositor;
    [Range(0f, 1f)]
    [SerializeField] float threshold = 0.5f;
    //VisualEffect visualEffect;
    GraphicsBuffer positionBuffer, facesPositionBuffer;
    int[] facesPixel;
    Vector2 textureDimension;
    int kernelIndex;
    List<Vector2> facesPositions = new List<Vector2>();

    public List<Vector2> faceFxPositions;

    public GameObject movableObject;

    public List<Vector2> GetAllFacePositions()
    {
        return faceFxPositions;
    }
    
    void Start()
    {
        //visualEffect = transform.GetComponent<VisualEffect>();
        textureDimension = new Vector2(compositor.GetMaskTexture().width, compositor.GetMaskTexture().height);
        int texturzeSize = (int)textureDimension.x * (int)textureDimension.y;
        positionBuffer = new GraphicsBuffer(GraphicsBuffer.Target.Structured, texturzeSize, sizeof(float) * 2);
        
        //buffer um die Pixel mit Gesichtern zu markieren
        facesPositionBuffer = new GraphicsBuffer(GraphicsBuffer.Target.Structured, texturzeSize, sizeof(int));
        facesPixel = new int[texturzeSize];

        kernelIndex = computeShader.FindKernel("AboveThreshold");    
        //visualEffect.SetGraphicsBuffer("bodyPixelBuffer", positionBuffer);
        //visualEffect.SetInt("BufferLength", texturzeSize);
    }

    void Update()
    {
        movableObject.transform.position = new Vector3(faceFxPositions[0].x, faceFxPositions[0].y, 0);
        computeShader.SetBuffer(kernelIndex, "outputPositions", positionBuffer);
        computeShader.SetBuffer(kernelIndex, "facePositions", facesPositionBuffer);
        computeShader.SetTexture(kernelIndex, "maskTexture", compositor.GetMaskTexture());
        computeShader.SetFloat("threshold", threshold);
        computeShader.SetInt("textureWidth", (int)textureDimension.x);
        computeShader.SetInt("textureHeight", (int)textureDimension.y);
        computeShader.Dispatch(kernelIndex, Mathf.CeilToInt(textureDimension.x / 8f), Mathf.CeilToInt(textureDimension.y / 8f), 1);

        //kopiere buffer von gpu zu cpu
        facesPositionBuffer.GetData(facesPixel);

        facesPositions.Clear();

        for (int i = 0; i < facesPixel.Length; i++)
        {
            //speichere alle Positionen die in der Textur mit 1 markiert sind als Gesichtspunkte
            if (facesPixel[i] == 1)
            {
                int x = i % (int)textureDimension.x;
                int y = i / (int)textureDimension.x;
                facesPositions.Add(new Vector2(x, y));
            }
        }

        //Ordne die Gesichtspunkte zu einzelnen Gesichtern
        List<List<Vector2>> faceCluster = ClusterPoints(facesPositions, 3f, 2);

        faceFxPositions = new List<Vector2>();

        for (int i = 0; i < faceCluster.Count; i++)
        {
            float x = 0;
            float y = 0;
            for (int ii = 0; ii < faceCluster[i].Count; ii++)
            {
                x += faceCluster[i][ii].x;
                y += faceCluster[i][ii].y;
            }

            //speichere den Mittelwert aller Gesichtspunkte
            x = (x / faceCluster[i].Count) * headsScaling.x + headsOffset.x;
            y = (y / faceCluster[i].Count) * headsScaling.y + headsOffset.y;
            faceFxPositions.Add(new Vector2(x, y));
        }

        //Übergebe die Koordinate an den FX Graph. Geht bestimmt noch eleganter. TODO: Nicht gefundene Gesichter müssten wieder zurückgesetzt werden...
        string [] fxGraphAttributes={"Head1","Head2","Head3","Head4","Head5"};
        for(int i=0;i<faceFxPositions.Count;i++)
        {
            if(i<=fxGraphAttributes.Length)
            {
               // Debug.Log(fxGraphAttributes[i]+" pos:"+faceFxPositions[i]);
               // Debug.DrawRay(new Vector3(faceFxPositions[i].x,faceFxPositions[i].y,0), Vector3.up * 2f, Color.red);    
               // visualEffect.SetVector2(fxGraphAttributes[i], faceFxPositions[i]);
            }
        }
    }

    List<List<Vector2>> ClusterPoints(List<Vector2> points, float epsilon, int minPts)
    {
        List<List<Vector2>> clusters = new List<List<Vector2>>();
        HashSet<Vector2> visited = new HashSet<Vector2>();

        foreach (Vector2 point in points)
        {
            if (!visited.Contains(point))
            {
                visited.Add(point);
                List<Vector2> neighbors = GetNeighbors(point, points, epsilon);

                if (neighbors.Count >= minPts)
                {
                    List<Vector2> cluster = ExpandCluster(point, neighbors, points, epsilon, minPts, visited);
                    clusters.Add(cluster);
                }
            }
        }
        return clusters;
    }

    List<Vector2> GetNeighbors(Vector2 point, List<Vector2> points, float epsilon)
    {
        List<Vector2> neighbors = new List<Vector2>();
        foreach (Vector2 otherPoint in points)
        {
            if (Vector2.Distance(point, otherPoint) <= epsilon)
            {
                neighbors.Add(otherPoint);
            }
        }
        return neighbors;
    }

    List<Vector2> ExpandCluster(Vector2 point, List<Vector2> neighbors, List<Vector2> points, float epsilon, int minPts, HashSet<Vector2> visited)
    {
        List<Vector2> cluster = new List<Vector2>();
        cluster.Add(point);

        foreach (Vector2 neighbor in neighbors)
        {
            if (!visited.Contains(neighbor))
            {
                visited.Add(neighbor);
                List<Vector2> newNeighbors = GetNeighbors(neighbor, points, epsilon);

                if (newNeighbors.Count >= minPts)
                {
                    List<Vector2> recursiveCluster = ExpandCluster(neighbor, newNeighbors, points, epsilon, minPts, visited);
                    cluster.AddRange(recursiveCluster);
                }
            }
            // Add the neighbor to the cluster if not already present
            if (!cluster.Contains(neighbor))
            {
                cluster.Add(neighbor);
            }
        }

        return cluster;
    }

    void OnDestroy()
    {
        if (positionBuffer != null)
            positionBuffer.Release();
    }

}
