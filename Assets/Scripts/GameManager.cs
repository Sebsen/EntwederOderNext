using UnityEngine;
using TMPro;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    [System.Serializable]
    public class QuestionData
    {
        public int LeftCounter;
        public int RightCounter;
    }
    [Header("Unity Objects")]
    public TMP_Text questionText;
    public TMP_Text answerText1;
    public TMP_Text answerText2;
    public TMP_Text counterLeftText;
    public int counterLeftCurrent = 0;
    public int counterRightCurrent = 0;
    public int totalCurrent = 0;

    public QuestionData currentData = new QuestionData();
    public TMP_Text counterRightText;
    public GameObject movableObject;
    public Collider leftSideArea;
    public Collider rightSideArea;
    public GameObject leftBackground;
    public GameObject rightBackground;
    public GameObject backgroundWouldYouRather;
    
    [Header("Tracker Settings")]
    public float movementThreshold = 0.001f;
    public float growthRate = 0.01f;  // Startwachstumsrate
    public float maxGrowthRate = 1.0f;  // Maximale Wachstumsrate
    public float growthAcceleration = 0.01f;  // Wachstumsbeschleunigung
    
    private Vector3 movableObjectOriginalScale;
    private Vector3 lastPosition;

    private float stationaryTime = 0f; 

    private string whichSide;

    private Vector3 Text1OriginalPosition;
    private Vector3 Text2OriginalPosition;




    public List<string> questions = new List<string> {
        "Entweder oder?",
        "Entweder oder?",
        //"Entweder oder?"
    };

    public List<string> answersLeft = new List<string> {
        "Ein Jahr ohne Internet",
        "Jeden Tag Party",
        /*"Nur noch Reality-TV schauen",
        "Unbegrenztes Geld, aber keine Freunde",
        "Berühmt für etwas Peinliches",
        "Immer die Wahrheit sagen müssen",
        "In der Zeit zurückreisen",
        "Nur noch per Fahrrad reisen",
        "Lebenslang kostenlose Konzerte",
        "Immer online sein",
        "Einmal den Mond besuchen",
        "Ein Buch über dein Leben veröffentlichen",*/
        
    };

    public List<string> answersRight = new List<string> {
        "Ein Jahr ohne dein Lieblingsessen",
        "Jeden Tag zu Hause bleiben",
        /*"Nur noch Dokumentationen schauen",
        "Kein Geld, aber viele Freunde",
        "Unbekannt bleiben dein ganzes Leben lang",
        "Nie wieder sprechen",
        "In die Zukunft reisen",
        "Nur noch fliegen",
        "Lebenslang kostenloses Essen",
        "Nie wieder online sein",
        "Einmal den tiefsten Punkt der Ozeane besuchen",
        "Ein Film über dein Leben",*/
    };

    private List<QuestionData> questionDataList = new List<QuestionData>();
    private int currentQuestionIndex = 0;

    private float leftSideTime = 0f;
    private float rightSideTime = 0f;

    private bool isDisplayingCounters = false;


    

    void Start()
    {
        movableObjectOriginalScale = movableObject.transform.localScale;
        lastPosition = movableObject.transform.position;
        Text1OriginalPosition = answerText1.transform.position;
        Text2OriginalPosition = answerText2.transform.position;
        


        InitializeQuestionData();
        LoadQuestion();
        counterLeftText.gameObject.SetActive(false);
        counterRightText.gameObject.SetActive(false);
    }

    void InitializeQuestionData()
    {
        foreach (var question in questions)
        {
            questionDataList.Add(new QuestionData());
        }
    }

    void Update()
    {
        if (isDisplayingCounters) return;

        float distanceMoved = Vector3.Distance(movableObject.transform.position, lastPosition);
        bool isMoving = distanceMoved > movementThreshold;


    if (isMoving)
    {
        // Schrumpft das Objekt, wenn es sich bewegt
        movableObject.transform.localScale = Vector3.Lerp(movableObject.transform.localScale, movableObjectOriginalScale, Time.deltaTime * 10f);

        stationaryTime = 0f;
        growthRate = 0.01f;
    }
    else // Wenn das Objekt stillsteht
    {
        stationaryTime += Time.deltaTime; // Erhöht die stillstehende Zeit
        if (leftSideArea.bounds.Contains(movableObject.transform.position) || rightSideArea.bounds.Contains(movableObject.transform.position))
        {
            if (movableObject.transform.localScale.x < 10f)
            {
                float timeFactor = Mathf.Pow((1f / 3f) * stationaryTime, 2); // Berechnung gemäß der Formel (((1/3) * x)^2)
                growthRate = 0.01f + growthAcceleration * timeFactor; // Nichtlineare Berechnung der Wachstumsrate
                growthRate = Mathf.Clamp(growthRate, 0.01f, maxGrowthRate); // Begrenzt die Wachstumsrate
                
                movableObject.transform.localScale += new Vector3(growthRate, 0.0f, growthRate) * Time.deltaTime;
            }
        }
        else
        {   
            // Setzt die Größe des Objekts zurück, wenn es sich nicht in einem der Bereiche befindet
            movableObject.transform.localScale = Vector3.Lerp(movableObject.transform.localScale, movableObjectOriginalScale, Time.deltaTime * 10f);

            Debug.Log("Growth Rate Reset");
            growthRate = 0.01f;
            stationaryTime = 0f;
        }
    }




        // Linke Seite
        if (leftSideArea.bounds.Contains(movableObject.transform.position))
        {
            leftSideTime += Time.deltaTime;
            rightSideTime = 0f;
            // Bewegt den Hintergrund nach vorne, bis eine bestimmte Position erreicht ist
            if (rightBackground.transform.position.z > 0.39f)
            {
                rightBackground.transform.position += new Vector3(0.0f, 0.0f, -0.1f) * Time.deltaTime;
                leftBackground.transform.position += new Vector3(0.0f, 0.0f, 0.1f) * Time.deltaTime;
            }


        } // Rechte Seite
        else if (rightSideArea.bounds.Contains(movableObject.transform.position))
        {
            rightSideTime += Time.deltaTime;
            leftSideTime = 0f;
            // Bewegt den Hintergrund nach vorne, bis eine bestimmte Position erreicht ist
            if(leftBackground.transform.position.z > 0.39f)
            {
                leftBackground.transform.position += new Vector3(0.0f, 0.0f, -0.1f) * Time.deltaTime;
                rightBackground.transform.position += new Vector3(0.0f, 0.0f, 0.1f) * Time.deltaTime;
            }
        } // Mittig
        else
        {   
            // Hintergründe zurücksetzen
            leftBackground.transform.position = new Vector3(-2.0f, 0.0f, 0.404f);
            rightBackground.transform.position = new Vector3(2.0f, 0.0f, 0.404f);
            
            leftSideTime = 0f;
            rightSideTime = 0f;


            // Bewegt den Hintergrund wieder zurück auf die Ursprungsposition
            //leftBackground.transform.position = new Vector3(-2.0f, 0.0f, 0.404f);
            //rightBackground.transform.position = new Vector3(2.0f, 0.0f, 0.404f);
            
            

        }

        if (leftSideTime > 3f && movableObject.transform.localScale.x > 2.5f)
        {
            //questionDataList[currentQuestionIndex].LeftCounter++;
            leftSideTime = 0f;
            whichSide = "left";
            StartCoroutine(DisplayCounters());
        }
        else if (rightSideTime > 3f && movableObject.transform.localScale.x > 2.5f)
        {
            //questionDataList[currentQuestionIndex].RightCounter++;
            rightSideTime = 0f;
            whichSide = "right";
            StartCoroutine(DisplayCounters());
        }
        lastPosition = movableObject.transform.position;
    }

    private IEnumerator DisplayCounters()
    {


        // movableObject x Größe + 1000
        movableObject.transform.localScale += new Vector3(1000.0f, 0.0f, 1000.0f);
        
        if(whichSide == "left")
        {
            Vector3 leftPos = leftBackground.transform.position;
            Vector3 rightPos = rightBackground.transform.position;

            leftBackground.transform.position = new Vector3(leftPos.x, leftPos.y, 0.39f);
            rightBackground.transform.position = new Vector3(rightPos.x, rightPos.y, 0.404f);
        }
        else if(whichSide == "right")
        {
            Vector3 leftPos = leftBackground.transform.position;
            Vector3 rightPos = rightBackground.transform.position;

            leftBackground.transform.position = new Vector3(leftPos.x, leftPos.y, 0.404f);
            rightBackground.transform.position = new Vector3(rightPos.x, rightPos.y, 0.39f);
        }
        

        isDisplayingCounters = true;

        questionText.gameObject.SetActive(false);
        backgroundWouldYouRather.gameObject.SetActive(false);
        //answerText1.gameObject.SetActive(false);
        //answerText2.gameObject.SetActive(false);

        // Current Counter Werte der jeweiligen Frage werde in Prozent zugewiesen

        // Debug.Log("Current Counter Left Old: " + counterLeftCurrent);
        



        // Debug.Log("Current Counter Left New: " + counterLeftCurrent);

        // Werte der aktuellen Frage abfragen 
        currentData = questionDataList[currentQuestionIndex];
        

        counterLeftCurrent = (int)((currentData.LeftCounter / (float)totalCurrent) * 100);
        counterRightCurrent = (int)((currentData.RightCounter / (float)totalCurrent) * 100);

        totalCurrent = (currentData.LeftCounter + currentData.RightCounter) + 1 ;

        int counterLeftTarget = 0;
        int counterRightTarget = 0;


        if(whichSide == "left")
        {
            counterLeftTarget = totalCurrent != 100 ? (int)(((currentData.LeftCounter + 1) / (float)totalCurrent) * 100) : 0;
            counterRightTarget = totalCurrent != 100 ? (int)(((currentData.RightCounter + 0)/ (float)totalCurrent) * 100) : 0;
        }
        else if(whichSide == "right")
        {
            counterLeftTarget = totalCurrent != 100 ? (int)(((currentData.LeftCounter + 0) / (float)totalCurrent) * 100) : 0;
            counterRightTarget = totalCurrent != 100 ? (int)(((currentData.RightCounter + 1) / (float)totalCurrent) * 100) : 0;
        }

        counterLeftText.gameObject.SetActive(true);
        counterRightText.gameObject.SetActive(true);

        Debug.Log("LeftTargetCounter:" + counterLeftTarget);
        Debug.Log("LeftCurrentCounter:" + counterLeftCurrent);
        Debug.Log("RightTargetCounter:" + counterRightTarget);
        Debug.Log("RightCurrentCounter:" + counterRightCurrent);

        // pro 10ms 1% von dem vorherigen Wert hinzufügen oder abziehen bis der Zielwert erreicht ist
        while( counterLeftCurrent != counterLeftTarget || counterRightCurrent != counterRightTarget)
        {
            if(counterLeftCurrent < counterLeftTarget)
            {
                counterLeftCurrent++;
            }
            else if(counterLeftCurrent > counterLeftTarget)
            {
                counterLeftCurrent--;
            }

            if(counterRightCurrent < counterRightTarget)
            {
                counterRightCurrent++;
            }
            else if(counterRightCurrent > counterRightTarget)
            {
                counterRightCurrent--;
            }

            counterLeftText.text = counterLeftCurrent.ToString() + "%";
            counterRightText.text = counterRightCurrent.ToString() + "%";

            yield return new WaitForSeconds(0.03f);
        }

        if(whichSide == "left")
        {
            currentData.LeftCounter++;
        }
        else if(whichSide == "right")
        {
            currentData.RightCounter++;
        }
        




        // counterLeftText.text = total != 0 ? (currentData.LeftCounter / (float)total * 100).ToString("F0") + "%" : "0%";
        //counterRightText.text = total != 0 ? (currentData.RightCounter / (float)total * 100).ToString("F0") + "%" : "0%";

        // counterLeftText.gameObject.SetActive(true);
        // counterRightText.gameObject.SetActive(true);

        AdjustPositions();

        yield return new WaitForSeconds(10f);
        
        float fadeOutAnimationDuration = 1.5f;
        // Startet die Fade-Out-Coroutines gleichzeitig
        Coroutine fadeLeftCoroutine = StartCoroutine(FadeOutText(counterLeftText, fadeOutAnimationDuration));
        Coroutine fadeRightCoroutine = StartCoroutine(FadeOutText(counterRightText, fadeOutAnimationDuration));
        Coroutine fadeAnswerText1 = StartCoroutine(FadeOutText(answerText1, fadeOutAnimationDuration));
        Coroutine fadeAnswerText2 = StartCoroutine(FadeOutText(answerText2, fadeOutAnimationDuration));
        Coroutine fadeBackground = StartCoroutine(FadeOutObject(movableObject, movableObjectOriginalScale, fadeOutAnimationDuration));

        // Wartet auf das Ende der Coroutines
        yield return fadeBackground;
        yield return fadeLeftCoroutine;
        yield return fadeRightCoroutine;

        

        // Question und Background wieder anzeigen
        questionText.gameObject.SetActive(true);
        backgroundWouldYouRather.gameObject.SetActive(true);

        // HumanTracker wieder auf Ursprungsgröße setzen
        movableObject.transform.localScale = movableObjectOriginalScale;

        isDisplayingCounters = false;

        LoadNextQuestion();



}

private IEnumerator FadeOutObject(GameObject obj, Vector3 originalScale, float duration)
{
    Renderer objRenderer = obj.GetComponent<Renderer>();
    if (objRenderer == null) yield break;

    Material objMaterial = objRenderer.material;
    Color originalColor = objMaterial.color;
    float currentTime = 0;

    // Fade-Out-Animation
    while (currentTime < duration)
    {
        currentTime += Time.deltaTime;
        float alpha = Mathf.Lerp(originalColor.a, 0f, currentTime / duration);
        objMaterial.color = new Color(originalColor.r, originalColor.g, originalColor.b, alpha);
        yield return null;
    }

    objMaterial.color = new Color(originalColor.r, originalColor.g, originalColor.b, originalColor.a); // Zurücksetzen der Alpha-Komponente
}



private IEnumerator FadeOutText(TMP_Text text, float duration)
{
    Color originalColor = text.color;
    float counter = 0;

    // Fade-Out-Animation
    while (counter < duration)
    {
        float alpha = Mathf.Lerp(1f, 0f, counter / duration);
        text.color = new Color(originalColor.r, originalColor.g, originalColor.b, alpha);
        counter += Time.deltaTime;
        yield return null;
    }

    text.gameObject.SetActive(false); // Deaktivieren des Text-Objekts nach dem Ausblenden

    text.color = new Color(originalColor.r, originalColor.g, originalColor.b, 1f); // Zurücksetzen der Alpha-Komponente
}

void LoadQuestion()
{
    // Setzt die Texte auf die Werte der aktuellen Frage
    questionText.text = questions[currentQuestionIndex];
    answerText1.text = answersLeft[currentQuestionIndex];
    answerText2.text = answersRight[currentQuestionIndex];

    // Aktiviert die Texte
    questionText.gameObject.SetActive(true);
    answerText1.gameObject.SetActive(true);
    answerText2.gameObject.SetActive(true);

    // Faded die Texte ein
    StartCoroutine(FadeInText(questionText, 2f));
    StartCoroutine(FadeInText(answerText1, 2f));
    StartCoroutine(FadeInText(answerText2, 2f));
}



void LoadNextQuestion()
{
    currentQuestionIndex = (currentQuestionIndex + 1) % questions.Count;
    LoadQuestion();

    // Zurücksetzen der Hintergrundpositionen
    ResetPositions();
}

private IEnumerator FadeInText(TMP_Text text, float duration)
{
    Color transparentColor = new Color(text.color.r, text.color.g, text.color.b, 0);
    Color originalColor = new Color(text.color.r, text.color.g, text.color.b, 1);
    float currentTime = 0;

    // Fade-In-Animation
    while (currentTime < duration)
    {
        currentTime += Time.deltaTime;
        text.color = Color.Lerp(transparentColor, originalColor, currentTime / duration);
        yield return null;
    }

    text.color = originalColor; // Zurücksetzen der Alpha-Komponente
}


void AdjustPositions()
{   

    // answerText1 um 150 nach oben verschieben
    answerText1.transform.position += new Vector3(0.0f , 150.0f, 0.0f);
    answerText2.transform.position += new Vector3(0.0f , 150.0f, 0.0f);


    Debug.Log("Adjust Background Positions");
    Vector3 leftPos = leftBackground.transform.position;
    Vector3 rightPos = rightBackground.transform.position;
    var currentData = questionDataList[currentQuestionIndex];
    int total = currentData.LeftCounter + currentData.RightCounter;

    

    float moveBackground = ((1f * currentData.LeftCounter / (float)total) + (-1f * currentData.RightCounter / (float)total));
    Debug.Log(moveBackground);

    rightBackground.transform.position = new Vector3((+2 + moveBackground), 0.0f, leftPos.z);
    leftBackground.transform.position = new Vector3((-2 + moveBackground), 0.0f, rightPos.z);
}

void ResetPositions()
{

    // answerText1 und answerText2 wieder auf die Ursprungsposition verschieben
    answerText1.transform.position = Text1OriginalPosition;
    answerText2.transform.position = Text2OriginalPosition;


    Debug.Log("Reset Background Positions");
    leftBackground.transform.position = new Vector3(-2.0f, 0.0f, 0.404f);
    rightBackground.transform.position = new Vector3(2.0f, 0.0f, 0.404f);
}


}





