using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameFunction : MonoBehaviour
{
    public Material material;

    void OnCollisionEnter(Collision collision)
    {
        material.color = Color.red; // Ändert die Farbe des Materials zu Rot
    }

    void OnCollisionExit(Collision collision)
    {
        material.color = Color.white; // Ändert die Farbe zurück, wenn die Kollision endet
    }
}